Problema de eventos en cells


Problema:
  Los eventos de cells, se estan duplicando el evento al navegar por la misma página con diferentes parametros de cabacera

Ejemplo :
  En esta app (Tenemos):
    - la ruta another?itemId=
    - un json (login.json)
    - el componente login
    - un componente another, con un botoón para actualizar los parametros de la misma sección
    - un json (another.json)
    - un controlador (Cross component)

    En el componente de login:
      Tenemos un botón que lanza un evento para ir al componente another (con nuevos parametros)

    En eljson login.json:
      Lo que hace es ir a la pagina de another con los parametros que se le mando 

    En el componente another:
      Tenemos un botón que lanza un evento para que actualice el itemId de manera aleatoria

    En el json another.json:
      Este cacha el evento y lo redirecciona al controlador

    En el controlador:
      Este recibe el evento que manda el json y lanza el nuevo evento para la actualización de la sección con los nuevos parametros

    Que problema ocasiona:
      Que la primera vez entramos, el componente lanza una vez su evento, y el controlador le llega 1 vez, pero si le damos al botón de nuevo, el componente solo lanza una vez el evento (lo cual es correcto), pero al controlador le llega dos veces, y entre mas le demos clic al botón mas entradas tiene el controlador.

  Uno real (Tenemos): 
    - la siguiente ruta creditCard/:idCard 
    - un json (creditCard.json), que maneja los eventos de la pagina.
    - un componente que tiene una lista de tarjetas, dos botones (atras y siguiente).
    - un controlador que se encarga de actualizar los parametros de la app y cambiar de sección (dentro de la misma pagina)

    En el componente de la lista:
      Tenemos una lista de tarjetas, cuando se le da clic en siguiente o atras ejecuta una acción (fire) indicando la tarjeta que quiere mostrar.

    El el json creditCard.json:
      Este cacha el evento que acaba de lanzar el componente con la tarjeta que necesita y lo redirecciona al controlador

    En el controlador: 
      Este cacha el evento que le mando el json y lanza un nuevo evento para que la misma pagina actualice la tarjeta seleccionada y con esto se cargue los nuevos datos.

    En el json creditCard.json:
      Cacha el nuevo evento y refresca la pagina con el nuevo parametro

    Que problema ocasiona:
      Que la primera vez entramos, el componente lanza una vez su evento, y el controlador le llega 1 vez, pero si le damos atras o adelante, el componente solo lanza una vez el evento (lo cual es correcto), pero al controlador le llega dos veces, y entre mas naveguemos mas entradas tiene el controlador.

¿Por que se hace así?
  El componente lanza un evento y el controlador es quien ejecuta la actualización, ya que el componente es genérico, por lo que no sabe en que sección se esta ejecutando (ya que solo contiene una lista de "algo"), por lo que solo avisa que requiere otra, y el controlador es quien ejecuta esa acción actualizar los parametros o quizas en otro ejemplo el controlador deba ir por datos y actualizar una tabla.


